'use strict';

var Helpers = {
    throttle: function (func, wait, options) {
        var context, args, result;
        var timeout = null;
        var previous = 0;
        var now = Date.now || function () {
            return new Date().getTime();
        }
        if (!options) options = {};
        var later = function () {
            previous = options.leading === false ? 0 : now();
            timeout = null;
            result = func.apply(context, args);
            if (!timeout) context = args = null;
        };
        return function () {
            var _now = now();
            if (!previous && options.leading === false) previous = _now;
            var remaining = wait - (_now - previous);
            context = this;
            args = arguments;
            if (remaining <= 0 || remaining > wait) {
                if (timeout) {
                    clearTimeout(timeout);
                    timeout = null;
                }
                previous = now;
                result = func.apply(context, args);
                if (!timeout) context = args = null;
            } else if (!timeout && options.trailing !== false) {
                timeout = setTimeout(later, remaining);
            }
            return result;
        }
    },
    debounce: function (func, wait, immediate) {
        var timeout, args, context, timestamp, result;
        var now = Date.now || function () {
            return new Date().getTime();
        }

        var later = function () {
            var last = now() - timestamp;

            if (last < wait && last >= 0) {
                timeout = setTimeout(later, wait - last);
            }
            else {
                timeout = null;
                if (!immediate) {
                    result = func.apply(context, args);
                    if (!timeout) { context = args = null; }
                }
            }
        }

        return function () {
            context = this;
            args = arguments;
            timestamp = now();
            var callNow = immediate && !timeout;
            if (!timeout) { timeout = setTimeout(later, wait); }
            if (callNow) {
                result = func.apply(context, args);
                context = args = null;
            }

            return result;
        }
    },
    prefixMethod: function (obj, method) {
        var pfx = ['webkit', 'moz', 'ms', 'o', ''];
        var p = 0, m, t;
        while (p < pfx.length && !obj[m]) {
            m = method;
            if (pfx[p] == '') {
                m = m.substr(0, 1).toLowerCase() + m.substr(1);
            }
            m = pfx[p] + m;
            t = typeof obj[m];
            if (t != 'undefined') {

                pfx = [pfx[p]];
                return (t == 'function' ? obj[m]() : obj[m]);
            }
            p++;
        }
    },
    isFullscreen: function () {
        return Helpers.prefixMethod(document, 'FullScreen') || Helpers.prefixMethod(document, 'IsFullScreen');
    },
    setFullscreen: function (el) {
        Helpers.prefixMethod(el, 'RequestFullScreen');
    },
    closeFullscreen: function () {
        return Helpers.prefixMethod(document, 'CancelFullScreen');
    }
};
(function ($) {

    $(document).ready(function () {
        var body = $('body');
        var html = $('html');
        var $window = $(window).eq(0);
        var $document = $(document).eq(0);
        var $body = $('body').eq(0);
        // animation
        var workItem = '.ih-b-work-item';
        var arrowDown = '.ih-arrow-down';

        initSliderFade();
        dropdownMenuJs();
        toggleMenu();
        initRelateSlider();

        fadeInUp(arrowDown);

        animationFadeInUp(workItem);

    // animation home page
        fadeInDown('.ih-menu-home li');

    // animation about page
        fadeInDown('.ih-about-title');
        fadeIn('.ih-s-about-hero .ih-body');
        animationFadeInUp('.ih-s-about-main .ih-row');
        animationFadeInUp('.ih-s-our-team p');

    // animation contact page

        rightToLeft('.ih-s-contact .ih-info');
        leftToRight('.ih-s-contact .ih-form');


    //js back to top
        $('.js-backtotop').on('click', function (e) {
            e.preventDefault();
            $('body, html').stop().animate({ scrollTop: 0 }, 1000);
        });

        // arrow down click
        $('.js-scroll-down').on('click', function (e) {
            e.preventDefault();
            $('body, html').stop().animate({ scrollTop: $('.ih-scroll-to').position().top }, 1000, 'swing');
        });

        $('.ih-s-home .ih-menu-home li a').hover(function(e){
            $('.ih-s-home').addClass('isHover');
        }, function(e){
            $('.ih-s-home').removeClass('isHover');
        })

        $('.js-title').each(function(){
            var me = $(this)
               , t = me.text().split(' ');
               if(t.length>1){
                me.html( t.shift()+'<strong> '+t.join(' ') + '</strong>');

               } else {
                me.html( '<strong> ' + t.shift() + ' '+t.join() + '</strong>');
               }
          });
        //   cateogry click
    
        $('.js-toggle-click').on('click', function (e) {
            e.preventDefault();
            $body.toggleClass('ih-dropdown-toggle');
        })
        $('html').click(function (e) {
            if ($(e.target).parents('.js-toggle-click-area').length == 0) {
                $body.removeClass('ih-dropdown-toggle');
            }
        });
        // detect screen size
        $body.layoutController({
            onSmartphone: function () {

            },
            onTablet: function () {

            },
            onDesktop: function () {
            }
        });

        // loading all images of the page
        $body.waitForImages(function () {
            $('.ih-b-loading-wrapper').fadeOut(function () {
                $(this).remove();
            });
        });

        // popup img
        $('.ih-img-popup').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            zoom: {
                enabled: true,
                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out' // CSS transition
            }
        });
        //popup for video
        $('.dd-iframe').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'iframe',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            zoom: {
                enabled: true,
                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out' // CSS transition
            }
        });


    }) // end document ready

        // animation
        function animationFadeInUp (element) {
            TweenMax.staggerFrom(element, 1, { opacity: 0, y: 50, delay: 0, ease: Power2.easeOut }, 0.4)
        }

        function fadeInUp(element){
            let sttFrom = {
                opacity: 0,
                y: 30,
                delay: 0.5,
                ease: Expo.easeOut
            };
            TweenMax.staggerFrom(element, 0.6, sttFrom);
        }

        function rightToLeft(element){
            let sttFrom = {
                opacity: 0,
                x: 30,
                delay: 0.5,
                ease: Expo.easeOut
            };
            TweenMax.staggerFrom(element, 0.6, sttFrom);
        }

        function leftToRight(element){
            var sttFrom = {
                opacity: 0,
                x: -30,
                delay: 0.5,
                ease: Expo.easeOut
            };
            TweenMax.staggerFrom(element, 0.6, sttFrom);
        }

        function fadeInDown(element){
            var sttFrom = {
                opacity: 0,
                y: -50,
                delay: 1,
                ease: Power4.easeOut
            };
            TweenMax.staggerFrom(element, 1, sttFrom, 0.2);
        }


        function fadeIn(element){
            var sttFrom = {
                opacity: 0,
                y: 5,
                delay: 0.5,
                ease: Power4.easeOut
            };
            TweenMax.staggerFrom(element, 1, sttFrom, 0.7);
        }


        function sliderFull(){
            $('.ih-b-slider-full').slick({
                dots: false,
                infinite: true,
                speed: 500,
                autoplay: true,
                autoplaySpeed: 3000,
                pauseOnHover: false,
                fade: true,
                cssEase: 'linear',
                prevArrow: '<div class="arrow-slider pre-nav"><span></span></div>',
                nextArrow: '<div class="arrow-slider next-nav"><span></span></div>'

            })
        }


        function initSliderFade(){
            $('.ih-slider-fade').slick({
                dots: false,
                infinite: true,
                speed: 500,
                autoplay: true,
                autoplaySpeed: 3000,
                fade: true,
                cssEase: 'linear',
                prevArrow: '<div class="arrow-slider pre-nav"><span></span></div>',
                nextArrow: '<div class="arrow-slider next-nav"><span></span></div>'
            });
        }

        function initRelateSlider(){
            $('.ih-relate-slider').slick({
                autoplay: true,
                autoplaySpeed: 6000,
                dots: true,
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 3,
                speed: 300,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            });
        }

        function toggleMenu(){
            $('.js-toggle-menu').on('click', function(e) {
                e.preventDefault();
                $('body').toggleClass('show-menu');
            })
        }

        function dropdownMenuJs(){
            $('.dropdown-toggle').on('click', function(e){
                e.preventDefault();
                $(this).next('.dropdown-menu').slideToggle(400);
                // $(this).next('.dropdown-menu').slideToggle();
            })
        }


        function msieversion() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
        
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
            {
                alert(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
            }
            else  // If another browser, return 0
            {
                alert('otherbrowser');
            }
        
            return false;
        }
        


})(jQuery);
