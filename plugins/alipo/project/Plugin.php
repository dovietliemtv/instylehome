<?php namespace Alipo\Project;

use Backend;
use BackendMenu;
use System\Classes\PluginBase;

/**
 * Project Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Project',
            'description' => 'No description provided yet...',
            'author'      => 'Alipo',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('Alipo.Project', 'project', 'plugins/alipo/project/partials/sidebar');
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Alipo\Project\Components\ProjectCp' => 'projectcp',
            'Alipo\Project\Components\ProjectDetailCp' => 'projectdetailcp',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'alipo.project.some_permission' => [
                'tab' => 'Project',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'project' => [
                'label'       => 'Project',
                'url'         => Backend::url('alipo/project/project'),
                'icon'        => 'icon-file-powerpoint-o',
                'permissions' => ['alipo.project.*'],
                'order'       => 500,
                'sideMenu' => [
                    'project' => [
                        'label'       => 'Project',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/project/project'),
                        'permissions' => ['alipo.cms.*'],
                        'group'       => 'Project'
                    ],
                    'newproject' => [
                        'label'       => 'Create new',
                        'icon'        => 'icon-plus',
                        'url'         => Backend::url('alipo/project/project/create'),
                        'permissions' => ['alipo.cms.*'],
                        'group'       => 'Project'
                    ],
                    'project' => [
                        'label'       => 'Projects',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/project/project/'),
                        'permissions' => ['alipo.cms.*'],
                        'group'       => 'Project'
                    ],
                    'category' => [
                        'label'       => 'Categories',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/project/projectcategory'),
                        'permissions' => ['alipo.cms.*'],
                        'group'       => 'Project'
                    ],
                    'service' => [
                        'label'       => 'Services',
                        'icon'        => 'icon-file-powerpoint-o',
                        'url'         => Backend::url('alipo/project/service'),
                        'permissions' => ['alipo.cms.*'],
                        'group'       => 'Project'
                    ],
    
                ]

            ],
        ];
    }
}
