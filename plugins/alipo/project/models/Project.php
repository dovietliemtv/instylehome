<?php namespace Alipo\Project\Models;

use Model;

/**
 * Project Model
 */
class Project extends Model
{
    use \October\Rain\Database\Traits\Validation;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_project_projects';
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
   public $translatable = [
       'title',
       'slug',
    //    'client',
       'type',
       'location',
       'about',
       'creative',
       'approach',
       'content',
   ];
    public $rules = [
        'title' => 'required',
        'slug' => 'required',
        // 'client' => 'required',
        'type' => 'required',
        'about' => 'required',
        'location' => 'required',
        'creative' => 'required',
        'approach' => 'required',
        'content' => 'required',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['content'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'categories' => [
            'Alipo\Project\Models\ProjectCategory',
            'table' => 'alipo_project_project_category_refs',
            'key'      => 'project_id'
        ],
        'services' => [
            'Alipo\Project\Models\Service',
            'table' => 'alipo_project_service_refs',
            'key'      => 'project_id'
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'thumb' => 'System\Models\File',
    ];
    public $attachMany = [];
}
