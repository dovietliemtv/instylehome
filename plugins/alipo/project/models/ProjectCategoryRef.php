<?php namespace Alipo\Project\Models;

use Model;

/**
 * ProjectCategoryRef Model
 */
class ProjectCategoryRef extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_project_project_category_refs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
