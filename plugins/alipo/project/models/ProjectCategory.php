<?php namespace Alipo\Project\Models;

use Model;

/**
 * ProjectCategory Model
 */
class ProjectCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_project_project_categories';
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'name',
        'slug',
    ];
    public $rules = [
        'name' => 'required',
    ];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'project' => [
            'Alipo\Project\Models\Projects',
            'table'    => 'alipo_project_project_category_refs',
            'key'      => 'project_category_id',
        ]

    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
