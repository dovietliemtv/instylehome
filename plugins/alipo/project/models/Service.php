<?php namespace Alipo\Project\Models;

use Model;

/**
 * Service Model
 */
class Service extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_project_services';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'service' => [
            'Alipo\Project\Models\Projects',
            'table'    => 'alipo_project_service_refs',
            'key'      => 'service_id',
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
