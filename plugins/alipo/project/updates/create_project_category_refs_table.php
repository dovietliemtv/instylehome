<?php namespace Alipo\Project\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProjectCategoryRefsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_project_project_category_refs')){
            Schema::create('alipo_project_project_category_refs', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('project_id')->unsigned();
                $table->integer('project_category_id')->unsigned();
                $table->primary(['project_id', 'project_category_id']);
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('alipo_project_project_category_refs');
    }
}
