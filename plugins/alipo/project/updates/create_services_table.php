<?php namespace Alipo\Project\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServicesTable extends Migration
{
    public function up()
    {

        if(!Schema::hasTable('alipo_project_services')){   
            Schema::create('alipo_project_services', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name')->nullable();
                $table->string('slug')->nullable()->index();
                $table->tinyInteger('is_active')->default(1);    
                $table->timestamps();
            });
        }


    }

    public function down()
    {
        Schema::dropIfExists('alipo_project_services');
    }
}
