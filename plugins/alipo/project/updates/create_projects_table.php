<?php namespace Alipo\Project\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProjectsTable extends Migration
{
    public function up()
    {
        Schema::create('alipo_project_projects', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('slug')->index();
            // $table->string('client');
            $table->string('type');
            $table->string('location');
            $table->text('about')->nullable();
            $table->text('creative')->nullable();
            $table->text('approach')->nullable();
            $table->longText('content')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('alipo_project_projects');
    }
}
