<?php namespace Alipo\Project\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateServiceRefsTable extends Migration
{
    public function up()
    {

        if(!Schema::hasTable('alipo_project_service_refs')){
            Schema::create('alipo_project_service_refs', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('project_id')->unsigned();
                $table->integer('service_id')->unsigned();
                $table->primary(['project_id', 'service_id']);
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_project_service_refs');
    }
}
