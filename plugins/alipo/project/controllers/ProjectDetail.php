<?php namespace Alipo\Project\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Project Detail Back-end Controller
 */
class ProjectDetail extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Alipo.Project', 'project', 'projectdetail');
    }
}
