<?php namespace Alipo\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAboutPagesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('alipo_cms_about_pages')){
            Schema::create('alipo_cms_about_pages', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('our_company_content');
                $table->text('our_commitment_content');
                $table->text('our_engagement_content');
                $table->text('our_process_content');
                $table->text('our_team_content');
                $table->text('our_story_content');
                $table->timestamps();
            });

        }
    }

    public function down()
    {
        Schema::dropIfExists('alipo_cms_about_pages');
    }
}
