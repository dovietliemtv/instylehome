<?php namespace Alipo\Cms\Models;

use Model;

/**
 * AboutPage Model
 */
class AboutPage extends Model
{
    use \October\Rain\Database\Traits\Validation;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_cms_about_pages';
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'our_company_content',
        'our_commitment_content',
        'our_engagement_content',
        'our_process_content',
        'our_team_content',
        'our_story_content',
    ];
    public $rules = [
        'our_company_content' => 'required',
        'our_commitment_content' => 'required',
        'our_engagement_content' => 'required',
        'our_process_content' => 'required',
        'our_team_content' => 'required',
        'our_story_content' => 'required',
    ];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'our_commitment_image' => 'System\Models\File',
        'our_engagement_image' => 'System\Models\File',
        'our_process_image' => 'System\Models\File',
    ];
    public $attachMany = [];
}
