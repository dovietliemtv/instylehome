<?php namespace Alipo\Cms\Models;

use Model;

/**
 * ContactPage Model
 */
class ContactPage extends Model
{
    use \October\Rain\Database\Traits\Validation;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'alipo_cms_contact_pages';
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];
    public $translatable = [
        'contact_info',
        'form_text',
    ];
    public $rules = [
        'contact_info' => 'required',
        'form_text' => 'required',
    ];
    
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['contact_info'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'background' => 'System\Models\File',
    ];
    public $attachMany = [];
}
